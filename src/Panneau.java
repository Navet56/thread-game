
import org.w3c.dom.css.Rect;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Panneau extends JPanel implements Runnable {
  private static int RECTHEIGHT = 10;
  private static int RECTWIDTH = 100;
  private final int height;
  private final int width;
  public static final int XFLIP = 1;
  public static final int YFLIP = 2;
  public static final int XYFLIP = 3;
  private final List<Rectangle> rects = new ArrayList<>();
  private final List<Thread> tasks = new ArrayList<>();
  public final List<Rectangle> waiters = new ArrayList<>();

  public Panneau(int width, int height) {
    this.width = width;
    this.height = height;
    Dimension size = new Dimension(width, height);
    this.setMinimumSize(size);
    this.setPreferredSize(size);
    this.setDoubleBuffered(true);
    this.resetRect();
  }

  public Rectangle getCollisionRect(Rectangle r1) {
    int x1, x2, y1, y2, x1max, y1max;
    for (Rectangle r2 : rects) {
      if (r1 != r2) { // on ne verifie bien evidemment pas si le rectangle est en collision avec lui meme
        x1 = r1.getX();
        x2 = r2.getX();
        y1 = r1.getY();
        y2 = r2.getY();
        y1max = y1 + RECTHEIGHT;
        x1max = x1 + RECTWIDTH;

        boolean xCollision = ((x1 < x2) && (x1max > x2)) || ((x1 > x2) && ((x1 - RECTWIDTH) < x2));

        boolean yCollision = (y1 < y2 && y1max > y2) || (y1 > y2 && y1 - RECTHEIGHT < y2);

        if (xCollision && yCollision) {
          return r2;
        }
      }
    }
    return null;
  }

  public synchronized void update () {
    this.repaint ();
  }

  public void paint (Graphics g) {
    Toolkit.getDefaultToolkit().sync();
    Graphics2D g2 = (Graphics2D) g;
    g2.setBackground (Color.WHITE);
    g2.clearRect (0, 0, width, height);

    for (Rectangle rect : rects) {
      g2.setColor(rect.getColor());
      g2.fillRect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());

    }
  }

  @Override
  public void run () {
    while(true){
      update();
    }
  }
  public void addRect (Color color) {
    Rectangle rect;
    do {
      if(color == null) color = getRandomColor();
      switch (color.getRGB()) {
        case -65281 ://Magenta : Vertical de 1
          rect = new Rectangle(this, getRandomNumber(0,width - RECTWIDTH), getRandomNumber(0,height - RECTHEIGHT), 0, 1, RECTHEIGHT, RECTWIDTH, color);
          break;
        case -16776961 ://Bleu : vertical vitesse aleatoire
          rect = new Rectangle(this, getRandomNumber(0,width - RECTWIDTH), getRandomNumber(0,height - RECTHEIGHT), 0, getRandomNumber(1,7), RECTHEIGHT, RECTWIDTH, color );
          break;
        case -16777216  ://Noir : Diagonal
          rect = new Rectangle(this, getRandomNumber(0,width - RECTWIDTH), getRandomNumber(0,height - RECTHEIGHT), getRandomNumber(1,7), -getRandomNumber(1,7), RECTHEIGHT, RECTWIDTH, color );
          break;
        case -16711936 ://Vert : horizontal vitesse aleatoire
          rect = new Rectangle(this, getRandomNumber(0,width - RECTWIDTH), getRandomNumber(0,height - RECTHEIGHT), getRandomNumber(1,7), 0, RECTHEIGHT, RECTWIDTH, color );
          break;
        case -256 ://Jaune : Diagonal inversé
          rect = new Rectangle(this, getRandomNumber(0,width - RECTWIDTH), getRandomNumber(0,height - RECTHEIGHT), -getRandomNumber(1,7), getRandomNumber(1,7), RECTHEIGHT, RECTWIDTH, color );
          break;
        default :
          rect = new Rectangle(this, getRandomNumber(0,width - RECTWIDTH), getRandomNumber(0,height - RECTHEIGHT), getRandomNumber(0,7), getRandomNumber(0,7), RECTHEIGHT, RECTWIDTH, color );
          break;
      }
    } while (getCollisionRect(rect) != null);
    this.rects.add(rect);
    this.tasks.add(new Thread(this.rects.get(this.rects.size() - 1), "" + (this.tasks.size() - 1)));
    this.tasks.get(this.tasks.size() - 1).start();
  }


  public void resetRect(){
    for (Rectangle rect : rects){
      rect.stopRunning();
    }
    this.rects.clear();
    this.addRect(Color.BLUE);
    this.addRect(Color.GREEN);
    this.addRect(Color.GREEN);
    this.addRect(Color.GREEN);
    this.addRect(Color.MAGENTA);
  }


  private static int getRandomNumber(int min, int max){
    return (int)(min + (Math.random() * (max - min)));
  }

  private static Color getRandomColor() {
    Color color;
    switch (getRandomNumber(0,6)) {
      case 1:
        color = Color.GRAY;
        break;
      case 2:
        color = Color.CYAN;
        break;
      case 3:
        color = Color.ORANGE;
        break;
      case 4:
        color = new Color(100,30,0); //BROWN
        break;
      case 5:
        color = Color.PINK;
        break;
      default:
        color = Color.RED;
    }
    return color;
  }

  public void delRect(Rectangle r) {
    if (rects.size() > 0){
      Rectangle rect = r;
      if (rect == null)  rect = rects.get(rects.size()-1);
      rect.stopRunning();
      this.rects.remove(rect);
    }
  }

  public synchronized void move(Rectangle rect) {

    // si le rectangle n'attend pas il essaie de bouger
    if (!rect.isWait()){
      int x = rect.getX();
      int y = rect.getY();
      // si le rectangle arrive en bord d'écran il essaie de rebondir
      if (x <= 0 || x + RECTWIDTH > width) {
        rect.flipXSpeed();
      }

      if (y <= 0 || y + RECTHEIGHT >= height) {
        rect.flipYSpeed();
      }

      rect.nextPos();

      Rectangle otherRect  = getCollisionRect(rect);
      if(otherRect != null) {
        // si le rectangle rentre en collision avec un autre rectangle il revient en arrière et s'arrete
        rect.previousPos();
        if (!this.waiters.contains(otherRect)) {
          this.waiters.add(rect);
          rect.setWait(true);
          try {
            wait();
          } catch (InterruptedException e){
            e.printStackTrace();
          }
        } else {
          int flipM = findFlipMethod(rect, otherRect);
          rect.flip(flipM);
          otherRect.flip(flipM);
        }
      }
      this.notifyRects();
    }
    
  }


  private int findFlipMethod(Rectangle r1, Rectangle r2) {
    int ret = 0;
    int x1, x2, y1, y2, x1max, y1max;
    x1 = r1.getX() + r1.getxSpeed();
    x2 = r2.getX() + r2.getxSpeed();
    y1 = r1.getY();
    y2 = r2.getY();
    y1max = y1 + RECTHEIGHT;
    x1max = x1 + RECTWIDTH;

    if (doesCollide(x1, x2, x1max, x1 - RECTWIDTH) && doesCollide(y1, y2, y1max, y1 - RECTHEIGHT)) ret += XFLIP;

    x1 = r1.getX();
    x2 = r2.getX();
    y1 = r1.getY() + r1.getySpeed();
    y2 = r2.getY() + r2.getxSpeed();
    y1max = y1 + RECTHEIGHT;
    x1max = x1 + RECTWIDTH;

    if (doesCollide(x1, x2, x1max, x1 - RECTWIDTH) && doesCollide(y1, y2, y1max, y1 - RECTHEIGHT)) ret += YFLIP;

    if (ret == 0) ret = XYFLIP;

    return ret;
  }

  private boolean doesCollide(int c1, int c2, int c1max, int c1min){
    return ((c1 <= c2) && (c1max >= c2)) || ((c1 >= c2) && ((c1min) <= c2));
  }

  public synchronized void notifyRects() {
    for(Rectangle rect : rects){
      rect.setWait(false);
      this.waiters.remove(rect);
    }
    this.notifyAll();
  }

  public void sizeRectIncr (boolean increment) {
    int i = increment ? 5 : -5 ;
    RECTHEIGHT+=i;
    RECTWIDTH+=i;
    for (Rectangle rect : rects){
      rect.setHeight(rect.getHeight()+i);
      rect.setWidth(rect.getWidth()+i);
    }

  }
}
