import java.awt.*;

/**
 *
 */
public class Rectangle implements Runnable{
    private final Panneau panneau;
    private int x;
    private int y;
    private int xSpeed;
    private int ySpeed;
    private int height;
    private int width;
    private Color color;
    private int panelWidth;
    private int panelHeight;
    private boolean wait = false;
    private volatile boolean running;

    public Rectangle(Panneau p, int xOrigin, int yOrigin, int xSpeed, int ySpeed, int height, int width, Color color) {
        this.panneau = p;
        this.x = xOrigin;
        this.y = yOrigin;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
        this.height = height;
        this.width = width;
        this.color = color;
        this.panelWidth = panneau.getWidth();
        this.panelHeight = panneau.getHeight();
        running = true;

    }
    public void flip(int flipM){
        if (flipM != 2) xSpeed *= -1;
        if (flipM != 1) ySpeed *= -1;

        if (x + xSpeed <= 0 || x + width + xSpeed > panelWidth) {
            x -= xSpeed;
        } else {
            x += xSpeed;
        }

        if (y + ySpeed <= 0 || y + height + ySpeed >= panelHeight) {
            y -= ySpeed;
        } else {
            y += ySpeed;
        }
    }

    public void setHeight (int height) {
        this.height = height;
    }

    public void setWidth (int width) {
        this.width = width;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getHeight() {
        return height;
    }

    public void flipXSpeed(){
        this.xSpeed = -xSpeed;
    }

    public void flipYSpeed(){
        this.ySpeed = -ySpeed;
    }

    public int getWidth() {
        return width;
    }

    public Color getColor() {
        return color;
    }

    public void stopRunning(){
        running = false;
    }

    public int getxSpeed () {
        return xSpeed;
    }

    public int getySpeed () {
        return ySpeed;
    }

    @Override
    public void run() {
        while(running) {
            panneau.move(this);
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void setWait(boolean b){
        this.wait = b;
    }

    public boolean isWait(){
        return wait;
    }

    public void nextPos(){
        x += xSpeed;
        y += ySpeed;
    }

    public void previousPos(){
        x -= xSpeed;
        y -= ySpeed;
    }

    @Override
    public String toString () {
        return "Rectangle{" +
                "x=" + x +
                ", y=" + y +
                ", xSpeed=" + xSpeed +
                ", ySpeed=" + ySpeed +
                ", color=" + color +
                '}';
    }
}
