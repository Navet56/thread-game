import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
* @author CEDSI n NAVET56
*/
public class Main {
 public static void main(String[] args) {
    if(args.length > 1) {
        testComponent("Thread game", new Panneau(new Integer(args[0]), new Integer(args[1])));
    } else {
        testComponent("Thread game", new Panneau(600, 600));
    }
 }

 public static final void testComponent (final String title, Panneau component) {
     SwingUtilities.invokeLater(() -> {
         JFrame jFrame = new JFrame(title);
         jFrame.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    System.exit(0);
                }
            });
         jFrame.getContentPane().add (component, BorderLayout.CENTER);
         SettingPanel settingPanel = new SettingPanel();
         settingPanel.getAddButton().addActionListener( e -> component.addRect(null));
         settingPanel.getDelButton().addActionListener( e -> component.delRect(null));
         settingPanel.getResetButton().addActionListener( e -> component.resetRect());
         settingPanel.getAddMagentaButton().addActionListener( e -> component.addRect(Color.MAGENTA));
         settingPanel.getAddBlueButton().addActionListener( e -> component.addRect(Color.BLUE));
         settingPanel.getAddBlackButton().addActionListener( e -> component.addRect(Color.BLACK));
         settingPanel.getAddGreenButton().addActionListener( e -> component.addRect(Color.GREEN));
         settingPanel.getAddYellowButton().addActionListener( e -> component.addRect(Color.YELLOW));
         settingPanel.getIncrSizeButton().addActionListener( e -> component.sizeRectIncr(true));
         settingPanel.getDecSizeButton().addActionListener( e -> component.sizeRectIncr(false));

         jFrame.add(settingPanel, BorderLayout.EAST);

         jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         jFrame.pack();
         Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
         Dimension size = jFrame.getSize();
         jFrame.setLocation ((screenSize.width - size.width)/4, (screenSize.height - size.height)/4);
         jFrame.setVisible(true);
         new Thread(component).start();

      });
    }
}
