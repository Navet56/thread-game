import javax.swing.*;
import java.awt.*;

public class SettingPanel extends JPanel {
    private final JButton addButton;
    private final JButton delButton;
    private final JButton resetButton;
    private final JButton addMagentaButton;
    private final JButton addBlueButton;
    private final JButton addGreenButton;
    private final JButton addYellowButton;
    private final JButton addBlackButton;
    private final JButton incrSizeButton;
    private final JButton decSizeButton;


    public SettingPanel(){
        this.addButton = new JButton("Ajout Rectangle Aleatoire");
        this.delButton = new JButton("Suppr Rectangle");
        this.resetButton = new JButton("Reset Rectangles");
        this.addMagentaButton = new JButton("Ajout Rectangle Magenta");
        this.addBlackButton = new JButton("Ajout Rectangle Noir");
        this.addBlueButton = new JButton("Ajout Rectangle Bleu");
        this.addGreenButton = new JButton("Ajout Rectangle Vert");
        this.addYellowButton = new JButton("Ajout Rectangle Jaune");
        this.incrSizeButton = new JButton("Taille++");
        this.decSizeButton = new JButton("Taille--");

        setLayout(new GridLayout(10,1));
        add(this.addButton);
        add(this.addMagentaButton);
        add(this.addBlueButton);
        add(this.addGreenButton);
        add(this.addBlackButton);
        add(this.addYellowButton);
        add(this.delButton);
        add(this.resetButton);
        add(this.incrSizeButton);
        add(this.decSizeButton);

    }

    public JButton getIncrSizeButton () {
        return incrSizeButton;
    }

    public JButton getDecSizeButton () {
        return decSizeButton;
    }

    public JButton getAddButton() {
        return addButton;
    }

    public JButton getDelButton() {
        return delButton;
    }

    public JButton getResetButton() {
        return resetButton;
    }

    public JButton getAddMagentaButton () {
        return addMagentaButton;
    }

    public JButton getAddBlueButton () {
        return addBlueButton;
    }

    public JButton getAddGreenButton () {
        return addGreenButton;
    }

    public JButton getAddYellowButton () {
        return addYellowButton;
    }

    public JButton getAddBlackButton () {
        return addBlackButton;
    }
}
