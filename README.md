# Utilisation de Thread en Java Swing
Par DIBERDER Eva et SIMAR Cedric
Groupe B 2020-2021
Projet réalisé lors du TP sur les activités (thread) en M3101 module Principes des systemes d'exploitations
Ce fichier est disponible en 3 versions : PDF, ODT et MARDOWN

## Run le projet :

`java -jar thread.jar`

Vous pouvez choisir la taille du bac à sable contenant les threads en tapant

`java -jar tp4.jar height width`

avec height = 800 et width = 600 par exemple :

`java -jar tp4.jar 800 600`

## Explications

### Explication générale

Vous pouvez voir au lancement de l'application, 5 rectangles de 3 types différents. Ces rectangles se déplacent dans un "bac à sable" (ou panneau). Vous pouvez ajouter ou retirer un rectangle du bac à sable grâce aux boutons situé à droite.
Les rectangles n'ont pas tous la même vitesse et se déplacent dans des directions différentes, ils peuvent donc entrer en collision !
Dans ce cas, un des 2 rectangles s'arrête pour laisser l'autre partir
Si les 2 entre en collision frontalement, aucun des 2 ne pourra se détacher de la situation en attendant -> il y a interbloquage. Dans ce cas, on les fait changer de direction, on "flip" leur direction.
Chaque rectangle vit sa propre vie, ils ont chacun leur activité ("thread")
Le programme est en boucle sans fin.

### Présentation des types de rectangles

Chaque rectangle a des propriétés différentes, vous pouvez ajouter chaque type de rectangle au bac a sable :

* Aléatoire : peux se déplacer soit verticalement, soit horizontalement, soit en diagonale
* Magenta : se déplace verticalement lentement (1 pixel par 1 pixel)
* Bleu : se déplace verticalement
* Vert : se déplace horizontalement
* Noir : se déplace en diagonal vers la droite
* Jaune : se déplace en diagonal vers la gauche

### Fonctionnalités :

Vous pouvez ajouter à votre guise n'importe quel type de rectangle grâce aux boutons situé à droite
Vous pouvez supprimer le dernier rectangle ajouté ainsi que revenir à la situation nominale (3 rectangles verts, 1 magenta et 1 bleu)
Vous pouvez également modifier la taille des rectangles pour voir ce que cela fait (fonction experimentale qui peut conduire à des bugs)

### Principe de synchronisation

Le panneau fait office de moniteur des rectangles, ce panneau est lancé dans un thread (il a donc une méthode run) et il hérite de JPanel.
Sa méthode run fait appelle à une méthode synchronized "update" qui repaint le panel (suppression des rectangles, redessinement des rectangles) en boucle.

Chaque rectangle est lancé dans un thread et possède egalement sa méthode run.
Cette méthode run appelle la méthode move du panel pour chaque rectangle et c'est cette méthode move qui fait tout le travail de deplacement/collision

### La méthode synchronisée move

Cette méthode move est synchronized car c'est elle qui va faire wait et notifyAll
Pour pouvoir bien fonctionner, nous avons mis un attribut booléen wait à chaque rectangle ainsi qu'une liste des rectangles en attente pour que le panneau puisse savoir qui est wait.

Si le rectangle veut bouger (donc qu'il n'est pas wait)
  alors on regarde s'il arrive au bord de l'ecran, dans ce cas on flip
  puis on le fait avancer (x++ et y++)
  puis on regarde si notre rectangle entre en collision avec un autre (méthode getCollisionRect())
  Si c'est le cas on fait revenir le rectangle à sa position initiale.
    Pour qu'il n'y est qu'un seule rectangle qui s'arrete, on regarde si l'autre rectangle est dans la liste des rectangles en attente, si c'est pas le cas on met notre rectangle en attente en l'ajoutant aux rectangles en attente et en utilisant la méthode wait() de la classe Object
    Si l'autre rectangle fait parti des rectangles en attente alors il y a interbloquage, on flip alors les 2 rectangles (voir findFlipMethod() et flip() )
  Puis on appelle la méthode notifyAll qui reveille tout les rectangles


### La méthode getCollisionRect

Cette méthode renvoie le rectangle en collision avec celui passé en parametre
On passe en revue tout les rectangles de la liste
Dès qu'il y a collision horizontale entre les 2 rectangles : (((x1 < x2) et (x1 + tailleRectangle > x2)) ou ((x1 > x2) et ((x1 - tailleRectangle) < x2)))
et qu'il y a collision verticale : (y1 < y2 && y1 + tailleRectangle > y2) et (y1 > y2 et y1 - tailleRectangle < y2)
on retourne le rectangle en collision.

sinon si à la fin de la boucle for qui passe en revue tout les rectangles, on n'a pas trouver de rectangle en collision, on retourne null, qui servira dans la méthode move

### Disclaimer
Le programme commence à donner ses limites à partir de 8 rectangles, il peut arriver ceci :
* 2 rectangles qui se chevauchent (commun)
* Plusieurs rectangles qui sont en interblocage car en collision multiple (peu commun)
* 1 rectangle se retrouve bloqué sur le bord car poussé par un autre (rare)
* Lenteurs (dépend du PC)
